<?php

// https://samelh.com/blog/2015/12/13/how-to-create-a-wordpress-settings-page-for-your-wordpress-plugin/

add_action('admin_menu', function() {
    add_options_page(
		'Donation Settings',
		'Donation Settings',
		'manage_options',
		'donation-settings',
		'donation_settings_cb'
    );
});

add_action( 'admin_init', function() {
    register_setting( 'donation-settings', 'map_option_1' );
    register_setting( 'donation-settings', 'map_option_2' );
    register_setting( 'donation-settings', 'map_option_3' );
    register_setting( 'donation-settings', 'map_option_5' );
    register_setting( 'donation-settings', 'map_option_6' );
    register_setting( 'donation-settings', 'map_option_7' );
    register_setting( 'donation-settings', 'map_option_8' );
    register_setting( 'donation-settings', 'map_option_9' );
    register_setting( 'donation-settings', 'map_option_10' );
    register_setting( 'donation-settings', 'map_option_11' );
    register_setting( 'donation-settings', 'map_option_12' );

    // general options
    register_setting( 'donation-settings', 'log' );
    register_setting( 'donation-settings', 'minimum_donation_amount');

    // email options
    register_setting( 'donation-settings', 'phpmailer_debug_mode' );
    register_setting( 'donation-settings', 'sender_address' );
    register_setting( 'donation-settings', 'sender_name' );
    register_setting( 'donation-settings', 'host' );
    register_setting( 'donation-settings', 'username' );
    register_setting( 'donation-settings', 'password' );
    register_setting( 'donation-settings', 'encryption' );
    register_setting( 'donation-settings', 'port' );
    register_setting( 'donation-settings', 'thankyou_email' );
    register_setting( 'donation-settings', 'notification_email' );
    register_setting( 'donation-settings', 'support_email' );

    // general gateway options
    register_setting( 'donation-settings', 'active_gateway' );
    register_setting( 'donation-settings', 'gateway_mode' );

    // stripe options
    register_setting( 'donation-settings', 'stripe_live_secret_key' );
    register_setting( 'donation-settings', 'stripe_live_publishable_key' );
    register_setting( 'donation-settings', 'stripe_test_secret_key' );
    register_setting( 'donation-settings', 'stripe_test_publishable_key' );

    // authdotnet options
    register_setting( 'donation-settings', 'authdotnet_live_api_id' );
    register_setting( 'donation-settings', 'authdotnet_live_transaction_key' );
    register_setting( 'donation-settings', 'authdotnet_sandbox_api_id' );
    register_setting( 'donation-settings', 'authdotnet_sandbox_transaction_key');

    // override options
    register_setting( 'donation-settings', 'custom_css' );
    register_setting( 'donation-settings', 'wrapper_element' );
});


function donation_settings_cb() {
  ?>
    <style>
    	.nav-tab {
    		cursor: pointer;
    	}

    	table {
    		border-collapse: separate;
   			border-spacing: 0 1em;
    	}

    	th.title-desc {
    		text-align: left !important;
    		padding-right: 75px;
    		width: 150px;
    	}

    	p.setting-desc {
    		position: relative;
    		margin-top: -12px;
    		color: #aaa;
    		/*width: 384px;*/ /* needs responsive width */
    	}

    	input[type=radio] {
    		padding-top: 300px;
    	}

        th.options-header {
            font-size: 20px;
            font-weight: 300;
            margin-bottom: -15px;
            text-align: left !important;
            padding-right: 100px;
            width: 150px;
        }

        section.link-content-section {
            display: none;
        }

        nav.nav-links-wrapper {
            position: relative;
            top: -25px;
            font-size: 14px;
            left: -226px;
        }
    </style>

    <div class="wrap">
      <form action="options.php" method="post">
        <h2>Donations Plugin</h2>

	    <h2 class="nav-tab-wrapper">
          <a class="nav-tab" id="provider-settings">Provider Settings</a>
	      <a class="nav-tab nav-tab-active" id="general-settings">General Settings</a>
	      <a class="nav-tab" id="form-settings">Form Settings</a>
          <a class="nav-tab" id="log">Log</a>

	    </h2>

        <?php
          settings_fields( 'donation-settings' );
          do_settings_sections( 'donation-settings' );
        ?>
        <br/>
        <table>

            <tbody class="tab-content-section log">
            <tr>
                <th></th>
                <td id="log">
                    <div id="display-window">
                    <?  // move into class and call method here
                        $file = fopen(get_home_path() . 'wp-content/plugins/donations/logs/logfile.txt',"r");
                        while(! feof($file)) {
                        echo fgets($file). "<br />";
                        }
                        fclose($file);
                    ;?>
                    </div>
                </td>
            </tr>

            </tbody>
            
            <!-- general settings -->
            <tbody class="tab-content-section general-settings">

                <tr>
                    <th></th>
                    <td>
                        <!-- link sections -->
                        <nav class="nav-links-wrapper">
                            <a class="nav-link" id="thankyou-email-template">Thank You Email</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class="nav-link" id="notification-email-template">Notification Email</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class="nav-link" id="support-email-template">Support Email</a>
                        </nav>
                    </td>
                </tr>

                <!-- thank you email template -->
                <section class="link-content-section thankyou-email-template">
                    <? wp_editor( get_option('thankyou_email'), 'thankyou_email', $settings = array('textarea_rows'=> '10', 'textarea_name' => 'thankyou_email') ); ?>
                </section>

                <!-- notification email template -->
                <section class="link-content-section notification-email-template">
                     <? wp_editor( get_option('notification_email'), 'notification_email', $settings = array('textarea_rows'=> '10', 'textarea_name' => 'notification_email') ); ?>
                </section>

                <!-- support email template -->
                <section class="link-content-section support-email-template">
                    <? wp_editor( get_option('support_email'), 'support_email', $settings = array('textarea_rows'=> '10', 'textarea_name' => 'support_email') ); ?>
                </section>

                <tr>
                    <th class="title-desc">Minimum Donation Amount</th>
                    <td><input type="text" placeholder="Minimum donation amount" name="minimum_donation_amount" value="<?php echo esc_attr( get_option('minimum_donation_amount') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>


                <tr>
                    <th class="title-desc">Sender Address</th>
                    <td><input type="text" placeholder="Sender Address" name="sender_address" value="<?php echo esc_attr( get_option('sender_address') ); ?>" size="50" /></td>
                </tr>
                <tr>
                	<th></th>
                    <td><p class="setting-desc">Typically, this is the same as the username.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Sender Name</th>
                    <td><input type="text" placeholder="Sender Name" name="sender_name" value="<?php echo esc_attr( get_option('sender_name') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Host</th>
                    <td><input type="text" placeholder="Host" name="host" value="<?php echo esc_attr( get_option('host') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Username</th>
                    <td><input type="text" placeholder="Username" name="username" value="<?php echo esc_attr( get_option('username') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Password</th>
                    <td><input type="password" placeholder="" name="password" value="<?php echo esc_attr( get_option('password') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Encryption</th>
                    <td><input type="text" placeholder="Encryption" name="encryption" value="<?php echo esc_attr( get_option('encryption') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Port</th>
                    <td><input type="text" placeholder="Port" name="port" value="<?php echo esc_attr( get_option('port') ); ?>" size="50" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>

                <tr>
                    <th class="title-desc">Debug Mode</th>
                    <td>
                        <select name="phpmailer_debug_mode">
                            <option value="">&mdash; select &mdash;</option>
                            <option value="on" <?php echo esc_attr( get_option('phpmailer_debug_mode') ) == 'on' ? 'selected="selected"' : ''; ?>>On</option>
                            <option value="off" <?php echo esc_attr( get_option('phpmailer_debug_mode') ) == 'off' ? 'selected="selected"' : ''; ?>>Off</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <th class="title-desc">Log</th>
                    <td><input type="text" placeholder="Path" name="map_option_7" value="<?php echo esc_attr( get_option('map_option_7') ); ?>" size="50" /></td>
                </tr>
                <tr>
                	<th></th>
                    <td><p class="setting-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p></td>
                </tr>
            </tbody>
 



            <!-- form settings -->
            <tbody class="tab-content-section form-settings">
            	<tr>
                    <th class="title-desc">Custom CSS</th>
                    <td><textarea placeholder="Custom CSS" name="custom_css" rows="5" cols="50"><?php echo esc_attr( get_option('custom_css') ); ?></textarea></td>
                </tr>
                <tr>
                    <th class="title-desc">Wrapper Element</th>
                    <td><input type="text" placeholder="Element" name="wrapper_element" value="<?php echo esc_attr( get_option('wrapper_element') ) ;?>" size="50" /></td>
                </tr>
            </tbody>



            <!-- provider settings -->
           <tbody class="tab-content-section provider-settings">
            <tr>
                <th class="options-header">Gateway Options</th>
            </tr>
            <tr>
                <th class="title-desc">Active Gateway</th>
                <td>
                    <label>
                        <input type="radio" name="active_gateway" value="stripe" <?php echo esc_attr( get_option('active_gateway') ) == 'stripe' ? 'checked="checked"' : ''; ?> /> Stripe <br/>
                    </label>
                    <label>
                        <input type="radio" name="active_gateway" value="authdotnet" <?php echo esc_attr( get_option('active_gateway') ) == 'authdotnet' ? 'checked="checked"' : ''; ?> /> Authorize.net
                    </label>
                </td>
            </tr>
            <tr>
                <th class="title-desc">Mode</th>
                <td>
                    <select name="gateway_mode">
                        <option value="">&mdash; select &mdash;</option>
                        <option value="test" <?php echo esc_attr( get_option('gateway_mode') ) == 'test' ? 'selected="selected"' : ''; ?>>Test</option>
                        <option value="live" <?php echo esc_attr( get_option('gateway_mode') ) == 'live' ? 'selected="selected"' : ''; ?>>Live</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th class="options-header">Stripe</th>
            </tr>
            <tr>
                <th class="title-desc">Live Secret Key</th>
                <td><input type="text" placeholder="" name="stripe_live_secret_key" value="<?php echo esc_attr( get_option('stripe_live_secret_key') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="title-desc">Live Publishable Key</th>
                <td><input type="text" placeholder="" name="stripe_live_publishable_key" value="<?php echo esc_attr( get_option('stripe_live_publishable_key') ); ?>" size="50" /></td>
            </tr>

            <tr>
                <th class="title-desc">Test Secret Key</th>
                <td><input type="text" placeholder="" name="stripe_test_secret_key" value="<?php echo esc_attr( get_option('stripe_test_secret_key') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="title-desc">Test Publishable Key</th>
                <td><input type="text" placeholder="" name="stripe_test_publishable_key" value="<?php echo esc_attr( get_option('stripe_test_publishable_key') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="options-header">Authorize.net</th>
            </tr>
            <tr>
                <th class="title-desc">Live API ID</th>
                <td><input type="text" placeholder="" name="authdotnet_live_api_id" value="<?php echo esc_attr( get_option('authdotnet_live_api_id') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="title-desc">Live Transaction Key</th>
                <td><input type="text" placeholder="" name="authdotnet_live_transaction_key" value="<?php echo esc_attr( get_option('authdotnet_live_transaction_key') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="title-desc">Sandbox API ID</th>
                <td><input type="text" placeholder="" name="authdotnet_sandbox_api_id" value="<?php echo esc_attr( get_option('authdotnet_sandbox_api_id') ); ?>" size="50" /></td>
            </tr>
            <tr>
                <th class="title-desc">Sandbox Transaction Key</th>
                <td><input type="text" placeholder="" name="authdotnet_sandbox_transaction_key" value="<?php echo esc_attr( get_option('authdotnet_sandbox_transaction_key') ); ?>" size="50" /></td>
            </tr>
           </tbody>
 
            <tr>
                <td><?php submit_button('Save All Changes'); ?></td>
            </tr>
 
        </table>
 
      </form>
    </div>

    <script>
    (function($, document, window, undefined){

    	// trigger click of first tab..
    	$(document).ready(function(){ $('.nav-tab').first().click() });

	    $('.nav-tab').on('click', function(e){
			$('.nav-tab').not(this).removeClass('nav-tab-active'); // remove active class from other tabs
			$(this).addClass('nav-tab-active'); // put active class on this tab
			$('tbody.tab-content-section').hide(); // hide content from other tabs
            $('section.link-content-section').hide(); // hide link sections
			$(`tbody.${e.target.id}`).show(); // show content for this tab
		});

        $('.nav-link').on('click', function(e){
            $('section.link-content-section').hide();
            $(`section.${e.target.id}`).show();
        });

    })(jQuery, document, window)
    </script>
  <?php
}
