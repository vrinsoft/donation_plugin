<!-- initial form -->
<link href="https://fonts.googleapis.com/css?family=Libre+Franklin" rel="stylesheet">
<form id="stripe-initial-form" novalidate class="stripe-initial-form">
  <div class="form-row amount">
    <label id="amount" class="amount-nvw">
      <span id="amount-lead">$</span>
      <input type="number" name="amount" required>
      <span id="amount-end">.00 USD</span>
    </label>
  </div>
  <div class="form-row monthly">
    <span class="monthly">Monthly</span>
    <input type="checkbox" checked name="recurring" value="monthly" id="recurring">
    <label for="recurring"></label>
  </div>
  <div class="form-row">
    <input type="submit" value="Donate" class="green-btn">
  </div>
</form>


<style>

  div#modal h4 {
    font-size: 14px;
    font-weight: bold;
    margin-top: -10px;
  }

  div#modal hr {
    margin-bottom: .8em;
    margin-top: -5px;
  }

  div.form-row.double-field {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    flex-wrap: nowrap;
    justify-content: space-between;
  }

  div.form-row.double-field label.firstname,
  div.form-row.double-field label.city,
  div.form-row.double-field label.country {
    margin-right: 10px;
    flex: 1;
  }

  div.form-row.double-field label.cc-num,
  div.form-row.double-field label.cardholder {
    margin-right: 10px;
    flex: 3;
  }

  div.form-row.double-field label.lastname,
  div.form-row.double-field label.cvc,
  div.form-row.double-field label.exp,
  div.form-row.double-field label.zip,
  div.form-row.double-field label.state {
    margin-right: 0;
    flex: 1;
  }

  div.form-row label span {
    font-weight: normal;
    font-size: 13px;
    color: gray;
  }

  div.form-row span.recognized-card {
    margin-left: 5px;
    color: darkblue;
  }

  div.form-row textarea {
    resize: none;
  }

/* modal form input placeholders */
div#modal div.form-row input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 12px; }
div#modal div.form-row input::-moz-placeholder { /* Firefox 19+ */
  font-size: 12px; }
div#modal div.form-row input:-ms-input-placeholder { /* IE 10+ */
  font-size: 12px; }
div#modal div.form-row input:-moz-placeholder { /* Firefox 18- */
  font-size: 12px; }

/* modal form textarea placeholders */
div#modal div.form-row textarea::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 12px; }
div#modal div.form-row textarea::-moz-placeholder { /* Firefox 19+ */
  font-size: 12px; }
div#modal div.form-row textarea:-ms-input-placeholder { /* IE 10+ */
  font-size: 12px; }
div#modal div.form-row textarea:-moz-placeholder { /* Firefox 18- */
  font-size: 12px; }


.ssl-message {
  color: gray;
}

</style>

<div id="modal">
  <button id="close-modal">x</button>
<!-- modal form -->
<form id="stripe-payment-form" method="post" class="nvw-stripe-payment-form">
  <span id="payment-errors"></span>

  <input type="hidden" name="amount">
  <input type="hidden" name="recurring">

  <!-- credit card info -->
  <h4>Credit Card Info</h4>
  <hr>

  <? if ( !empty($_SERVER['HTTPS']) ): ?>
    <h4 class="ssl-message">
      <i class="fa fa-lock" aria-hidden="true"></i>
       &nbsp;This is a secure SSL encrypted payment.
    </h4>
  <? else: ?>
    <h4 class="ssl-message">
      <i class="fa fa-unlock-alt" aria-hidden="true"></i>
      &nbsp;This payment is not SSL encrypted.
    </h4>
  <? endif; ?>

  <div class="form-row double-field">
    <label class="cc-num">
      <span>Card Number</span> *<span class="recognized-card"></span>
      <input type="text" placeholder="Card Number" class="stripe-cc" data-stripe="number">
    </label>
    <label class="cvc">
      <span>CVC</span> *
      <input type="text" placeholder="CVC" data-stripe="cvc" name="cvc">
    </label>
  </div>

  <div class="form-row double-field">
    <label class="cardholder">
      <span>Name on the Card</span> *
      <input type="text" placeholder="Card Name" class="cardholder" data-stripe="cardholder" name="cardholder">
    </label>
    <label class="exp">
      <span>Expiration</span> *
      <input type="text" placeholder="MM / YYYY" data-stripe="exp" class="stripe-exp" name="exp">
    </label>
  </div>
  <div class="nvw-billing-form">
  <!-- billing info -->
  <h4>Billing Details</h4>
  <hr>
  <div class="form-row">
    <label class="address-1">
      <span>Address 1</span> *
      <input type="text" placeholder="Address line 1" data-stripe="address-1" name="address-1">
    </label>
  </div>
    <div class="form-row">
    <label class="address-2">
      <span>Address 2</span>
      <input type="text" placeholder="Address line 2" data-stripe="address-2" name="address-2">
    </label>
  </div>
  <div class="form-row double-field">
    <label class="city">
      <span>City</span> *
      <input type="text" placeholder="City" class="city" data-stripe="city">
    </label>
    <label class="zip">
      <span>Zip / Postal Code</span> *
      <input type="text" placeholder="Zip / Postal Code" data-stripe="zip" class="stripe-zip" name="zip">
    </label>
  </div>
  <div class="form-row double-field">
    <label class="country">
      <span>Country</span> * <?php //echo $url = DONATIONS_PLUGIN_DIR; ?>
      <select placeholder="Country" class="country" data-stripe="country">
        <? include('country-options.php'); ?>
		<!--<option value="CA">Canada</option>-->
      </select>
	  
    </label>
    <label class="state">
      <span>State / Province</span> *
      <input type="text" placeholder="State / Province" data-stripe="state" class="stripe-state" name="state">
    </label>
  </div>
  </div>

  <div class="nvw-personalinfo-form">
  <!-- personal info -->
  <h4>Personal Info</h4>
  <hr>
<!--   <div class="form-row double-field">
    <label class="firstname">
      <span>First Name</span> *
      <input type="text" placeholder="First Name" data-stripe="firstname" name="firstname">
    </label>
    <label class="lastname">
      <span>Last Name</span> *
      <input type="text" placeholder="Last Name" data-stripe="lastname" name="lastname">
    </label>
  </div>
 -->
  <div class="form-row">
    <label class="email">
      <span>Email Address</span> *
      <input type="text" placeholder="Email Address" data-stripe="email" name="email_address">
    </label>
  </div>
    <div class="form-row">
    <label class="notes">
      <span>Note</span>
      <textarea placeholder="Note" data-stripe="note" name="note"></textarea>
    </label>
  </div>
  </div>

<!-- 
  <div class="form-row">
    <label>
      <span>Expiration (MM/YY)</span>
      <input type="text" size="2" data-stripe="exp_month" name="exp_month">
    </label>
    <span> / </span>
    <input type="text" size="2" data-stripe="exp_year" name="exp_year">
  </div>

  <div class="form-row">
    <label>
      <span>Billing ZIP Code</span>
      <input type="text" size="6" data-stripe="address_zip" name="address_zip">
    </label>
  </div>
 -->

  <?php wp_nonce_field( 'stripe_nonce', 'stripe_nonce' ); ?>

  <input type="submit" class="submit-stripe green-btn" id="submit-stripe" value="Submit Payment">

  </form>
</div>>
  </form>
</div>