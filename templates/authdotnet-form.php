<!-- initial form -->
<link href="https://fonts.googleapis.com/css?family=Libre+Franklin" rel="stylesheet">
<form id="autho-initial-form" novalidate class="autho-initial-form">
<span id = "display_message"></span>
  <div class="form-row amount">
    <label id="amount" class="amount-nvw">
      <span id="amount-lead">$</span>
      <input type="number" id="amouint" name="amount" required>
      <span id="amount-end">.00 USD</span>
    </label>
  </div>
  <div class="form-row monthly monthly-chbox">
    <span class="monthly">Monthly</span>
    <input type="checkbox" checked name="recurring" value="monthly" id="recurring">
    <label for="recurring"></label>
  </div>
  <div class="form-row">
    <input type="submit" value="Donate" class="green-btn">
  </div>
</form>


<style>

  div#modal h4 {
    font-size: 14px;
    font-weight: bold;
    margin-top: -10px;
  }

  div#modal hr {
    margin-bottom: .8em;
    margin-top: -5px;
  }

  div.form-row.double-field {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    flex-wrap: nowrap;
    justify-content: space-between;
  }

  div.form-row.double-field label.firstname,
  div.form-row.double-field label.city,
  div.form-row.double-field label.country {
    margin-right: 10px;
    flex: 1;
  }

  div.form-row.double-field label.cc-num,
  div.form-row.double-field label.cardholder {
    margin-right: 10px;
    flex: 3;
  }

  div.form-row.double-field label.lastname,
  div.form-row.double-field label.cvc,
  div.form-row.double-field label.exp,
  div.form-row.double-field label.zip,
  div.form-row.double-field label.state {
    margin-right: 0;
    flex: 1;
  }

  div.form-row label span {
    font-weight: normal;
    font-size: 13px;
    color: gray;
  }

  div.form-row span.recognized-card {
    margin-left: 5px;
    color: darkblue;
  }

  div.form-row textarea {
    resize: none;
  }

/* modal form input placeholders */
div#modal div.form-row input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 12px; }
div#modal div.form-row input::-moz-placeholder { /* Firefox 19+ */
  font-size: 12px; }
div#modal div.form-row input:-ms-input-placeholder { /* IE 10+ */
  font-size: 12px; }
div#modal div.form-row input:-moz-placeholder { /* Firefox 18- */
  font-size: 12px; }

/* modal form textarea placeholders */
div#modal div.form-row textarea::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size: 12px; }
div#modal div.form-row textarea::-moz-placeholder { /* Firefox 19+ */
  font-size: 12px; }
div#modal div.form-row textarea:-ms-input-placeholder { /* IE 10+ */
  font-size: 12px; }
div#modal div.form-row textarea:-moz-placeholder { /* Firefox 18- */
  font-size: 12px; }


.ssl-message {
  color: gray;

}

.autho-initial-form{width: 340px; text-align: center; margin: 0 auto; -webkit-box-shadow: 0 0 15px 1px rgba(0,0,0,0.1);
-moz-box-shadow: 0 0 15px 1px rgba(0,0,0,0.1);
-ms-box-shadow: 0 0 15px 1px rgba(0,0,0,0.1);
-o-box-shadow: 0 0 15px 1px rgba(0,0,0,0.1);
box-shadow: 0 0 15px 1px rgba(0,0,0,0.1); padding: 35px 30px 30px 30px;}
.autho-initial-form input#amouint{float: left; width: 110px; margin: 0 10px;}
.autho-initial-form span#amount-lead{float: left; line-height: 43px;}
.autho-initial-form span#amount-end{float: left; line-height: 43px;}
.autho-initial-form .amount{display: table; margin: 0 auto; }
.autho-initial-form .monthly-chbox{clear: both; text-align: left; margin: 25px 0 25px 34px; padding: 0 0 0 16px; position: relative; }
.autho-initial-form .monthly-chbox{}
.autho-initial-form .monthly-chbox input[type="checkbox"] + label::after{left: 0; top: 0;}
.autho-initial-form .monthly-chbox input[type="checkbox"] + label::before{left: 0; top: 0;}
.autho-initial-form .monthly-chbox input[type="checkbox"] + label{position: initial;}
.autho-initial-form #error-msg{padding: 0 0 10px 0; font-size: 14px; color: #f00 !important; }
select{width: 100%; height: 30px; background: #f7f7f7; border: 1px solid #d1d1d1; font-size: 12px; color: #686868; padding: 0 25px 0 8px; }
button#close-modal{position: absolute; right: 8px; top: 8px;}
span.error-creditcard, span.error-redcolor{color: #f00 !important; font-size: 14px; }


</style>

<div id="modal">
  <button id="close-modal">x</button>
<!-- modal form -->
<form id="authorize-payment-form" class="nvw-stripe-payment-form">
  <span id="payment-errors"></span>

  <input type="hidden" name="amount">
  <input type="hidden" name="recurring">

  <!-- credit card info -->
  <h4>Credit Card Info</h4>
  <hr>

  <? if ( !empty($_SERVER['HTTPS']) ): ?>
    <h4 class="ssl-message">
      <i class="fa fa-lock" aria-hidden="true"></i>
       &nbsp;This is a secure SSL encrypted payment.
    </h4>
  <? else: ?>
    <h4 class="ssl-message">
      <i class="fa fa-unlock-alt" aria-hidden="true"></i>
      &nbsp;This payment is not SSL encrypted.
    </h4>
  <? endif; ?>

  <div class="form-row double-field">
    <label class="cc-num">
      <span>Card Number</span> *<span class="recognized-card"></span>
      <input type="text" name="creditcard" id="creditcard" placeholder="Card Number" class="stripe-cc" data-authdotnet="number">
      <span class="error-creditcard error-redcolor"></span>
    </label>
    <label class="cvc">
      <span>CVC</span> *
      <input type="text" placeholder="CVC" data-authdotnet="cvc" name="cvc" id="cvc" >
	  <span class="error-cvc error-redcolor"></span>
    </label>
  </div>

  <div class="form-row double-field">
    <label class="cardholder">
      <span>Name on the Card</span> *
      <input type="text" placeholder="Card Name" class="cardholder" name="cardholder" id="cardholder" >
	  <span class="error-cardholder error-redcolor"></span>
    </label>
    <label class="exp">
      <span>Expiration</span> *
      <input type="text" placeholder="MM / YYYY" class="stripe-exp" name="exp" id="exp" data-authdotnet="exp">
	  <span class="error-exp error-redcolor"></span>
    </label>
  </div>
  <div class="nvw-billing-form">
  <!-- billing info -->
  <h4>Billing Details</h4>
  <hr>
  <div class="form-row">
    <label class="address-1">
      <span>Address 1</span> *
      <input type="text" placeholder="Address line 1" name="address-1" id="address" data-authdotnet="address-1">
	  <span class="error-address error-redcolor"></span>
    </label>
  </div>
    <div class="form-row">
    <label class="address-2">
      <span>Address 2</span>
      <input type="text" placeholder="Address line 2" name="address-2" data-authdotnet="address-2">
    </label>
  </div>
  <div class="form-row double-field">
    <label class="city">
      <span>City</span> *
      <input type="text" placeholder="City" class="city" name="city" id="city">
	  <span class="error-city error-redcolor"></span>
    </label>
    <label class="zip">
      <span>Zip / Postal Code</span> *
      <input type="text" placeholder="Zip / Postal Code" class="stripe-zip" name="zip" id="zip" data-authdotnet="zip">
	  <span class="error-zip error-redcolor"></span>
    </label>
  </div>
  <div class="form-row double-field">
    <label class="country">
      <span>Country</span> * <?php //echo $url = DONATIONS_PLUGIN_DIR; ?>
      <select placeholder="Country" class="country" name="country" id="country">
        <?php include('country-options.php'); ?>
		<!--<option value="CA">Canada</option>-->
      </select>
	   <span class="error-country error-redcolor"></span>
    </label>
    <label class="state">
      <span>State / Province</span> *
      <input type="text" placeholder="State / Province" class="stripe-state" name="state" id="state">
	  <span class="error-state error-redcolor"></span>
    </label>
  </div>
  </div>

  <div class="nvw-personalinfo-form">
  <!-- personal info -->
  <h4>Personal Info</h4>
  <hr>
   <div class="form-row">
    <label class="email">
      <span>First Name</span> *
      <input type="text" placeholder="First Name" name="first_name" id="firstname">
	  <span class="error-firstname error-redcolor"></span>
    </label>
  </div>
    <div class="form-row">
    <label class="email">
      <span>Last Name</span> *
      <input type="text" placeholder="Last Name" name="last_name" id="lastname">
	   <span class="error-lastname error-redcolor"></span>
    </label>
  </div>
  <div class="form-row">
    <label class="email">
      <span>Email Address</span> *
      <input type="text" placeholder="Email Address" name="email_address" id="email">
	   <span class="error-email error-redcolor"></span>
    </label>
  </div>
    <div class="form-row">
    <label class="notes">
      <span>Note</span>
      <textarea placeholder="Note" name="note"></textarea>
    </label>
  </div>
  </div>

<!-- 
  <div class="form-row">
    <label>
      <span>Expiration (MM/YY)</span>
      <input type="text" size="2" data-stripe="exp_month" name="exp_month">
    </label>
    <span> / </span>
    <input type="text" size="2" data-stripe="exp_year" name="exp_year">
  </div>

  <div class="form-row">
    <label>
      <span>Billing ZIP Code</span>
      <input type="text" size="6" data-stripe="address_zip" name="address_zip">
    </label>
  </div>
 -->

  <?php //wp_nonce_field( 'stripe_nonce', 'stripe_nonce' ); ?>

  <input type="submit" class="submit-stripe green-btn" id="submit-authorize" value="Submit Payment">

  </form>
</div>>
  </form>
</div>
