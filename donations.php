<?php
/*
Plugin Name: CYP Donations
Plugin URL: https://shmulimarkel.com/
Description: Payment gateways for donations
Version: 0.1
Author: Shmuli Markel
Author URI: https://shmulimarkel.com/
*/


// exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( !class_exists('Donations') ) {

	final class Donations {

		// singleton Donations instance
		protected static $_instance;

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		public function __construct() {
			$this->setup_constants();
			$this->include_scripts();
		}

		public function setup_constants() {
			// plugin version
			if ( ! defined( 'DONATIONS_VERSION' ) ) {
				define( 'DONATIONS_VERSION', '1.0' );
			}
			// plugin folder path
			if ( ! defined( 'DONATIONS_PLUGIN_DIR' ) ) {
				define( 'DONATIONS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
			}
			// plugin folder url
			if ( ! defined( 'DONATIONS_PLUGIN_URL' ) ) {
				define( 'DONATIONS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
			}
			// plugin basename aka: "donations/donations.php"
			if ( ! defined( 'DONATIONS_PLUGIN_BASENAME' ) ) {
				define( 'DONATIONS_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
			}
			// plugin root file
			if ( ! defined( 'DONATIONS_PLUGIN_FILE' ) ) {
				define( 'DONATION_PLUGIN_FILE', __FILE__ );
			}
		}

		public function include_scripts() {
			require 'functions.php';
			require 'functions-authdotnet.php';
			require 'functions-stripe.php';

			// testing
			if ( get_option('active_gateway')  == 'authdotnet' ) {
				//
			} elseif ( get_option('active_gateway')  == 'stripe' ){
				//
			}

		}

	}

}

// invoke instance singleton
Donations::instance();