<?php

if ( !class_exists('Stripe') ) require 'vendor/autoload.php';
require 'config-stripe.php';
require 'charge-stripe.php';

// [stripe_payment_form]
function stripe_payment_form_cb(){
	wp_enqueue_style( 'donation-styles', plugins_url() . '/donations/templates/styles.css' );
	wp_enqueue_style( 'select-boxit-styles', 'http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css' );
	require 'templates/stripe-form.php';
	wp_enqueue_script( 'stripe-v2-js', 'https://js.stripe.com/v2/' );
	wp_enqueue_script( 'stripe-v3-js', 'https://js.stripe.com/v3/' );
	wp_enqueue_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', array('jquery') );
	wp_enqueue_script( 'select-boxit-js', 'http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js' );
	wp_enqueue_script( 'cleave-js', plugins_url() . '/donations/js/cleave.min.js' );
	wp_enqueue_script( 'donations-fontawesome', 'https://use.fontawesome.com/446ba55cfa.js' );
	wp_enqueue_script( 'form-submission-js', plugins_url() . '/donations/js/submit-form-stripe.js' );

	// get publishable key of active mode (default: test mode)
	$publishable_key = (get_option('gateway_mode') != 'live')
		? get_option('stripe_test_publishable_key')
		: get_option('stripe_live_publishable_key');

	// get minimum donation amount (default: $2.00)
	$minimum_amount = ( empty(get_option('minimum_donation_amount')) )
		? 2 : get_option('minimum_donation_amount');

	// get wrapper element to append screen overlay (default: #page)
	$wrapper_element = ( empty( get_option('wrapper_element')) )
		? '#page' : get_option('wrapper_element');

	// pass `ajaxurl`, `publishable_key` and `minimum_amount` variables to js.
	wp_localize_script( 'form-submission-js', 'stripe', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'publishable_key' => $publishable_key,
		'minimum_amount'  => $minimum_amount,
		'wrapper_element' => $wrapper_element
	));
}
add_shortcode( 'stripe_payment_form', 'stripe_payment_form_cb' );