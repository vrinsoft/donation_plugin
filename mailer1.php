<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//require 'vendor/autoload.php';

class Mail extends PHPMailer {

	function __construct() {
		$this->SMTPDebug = ( get_option('phpmailer_debug_mode') != 'off' ) ? 2 : 0; // default: on
		$this->isSMTP();
		$this->SMTPAuth = true;
		$this->Host = get_option('host');
		$this->Username = get_option('username');
		$this->Password = get_option('password');
		$this->SMTPSecure = get_option('encryption');
		$this->Port = get_option('port');
	}

	function transmit($type, $data) {
		try {

			// parse array into variables
			extract($data);

			// sender and recipient
		    $this->setFrom('shmuel.stack@gmail.com', get_option('sender_name'));
		    $this->addAddress($donor_email, $firstname .' '. $lastname);

		    // {firstname}, {lastname}, {fullname}, {amount}
		    // replace placeholders with variables
		    $message = get_option($type.'_email');
		    $message = str_replace( '{firstname}', $firstname, $message );
			$message = str_replace( '{lastname}', $lastname, $message );
			$message = str_replace( '{fullname}', $firstname .' '. $lastname, $message );
			$message = str_replace( '{amount}', '$'.$amount.'.00', $message );

		    // email content
		    $this->isHTML(true);
		    $this->Subject = 'Subject here..';
		    $this->Body    = wpautop($message);
		    $this->AltBody = 'This is the body in plain text for non-HTML mail clients';

		    $this->send();
		    echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
	}
}