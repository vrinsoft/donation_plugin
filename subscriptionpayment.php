<?php
//require_once(dirname(__FILE__).'../../../../wp-config.php'); 
require_once "vendor/autoload.php";
require_once "vendor/sdk-php-master/autoload.php";
error_reporting(E_ALL);

//require_once "constants.php";
//require_once('mailer1.php');
//require_once('logger.php');

parse_str( json_decode(stripslashes($_POST['formData']), TRUE) );


define('ANET_LOGIN_ID', $publishable_key);
define('ANET_TRANSACTION_KEY', $transaction_key);



use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
  date_default_timezone_set('America/Los_Angeles');
//add_action( 'wp_ajax_nopriv_createSubscription', 'createSubscription' );
//add_action( 'wp_ajax_createSubscription', 'createSubscription' );

  $modechange = $mode; 


 if ($recurring == "") {
	
	$post_values = array(
"x_login" => $publishable_key,
"x_tran_key" => $transaction_key,
"x_version" => "3.1",
"x_delim_data" => "TRUE",
"x_delim_char" => "|",
"x_relay_response" => "FALSE",
//"x_market_type" => "2",
"x_device_type" => "1",
"x_type" => "AUTH_CAPTURE",
"x_method" => "CC",
"x_invoice_num" => time(),
"x_card_num" => $creditcard,
//"x_exp_date" => "0115",
"x_exp_date" => $exp,
"x_amount" => $amount,
//"x_description" => "Sample Transaction",
"x_first_name" => $first_name,
"x_last_name" => $last_name,
"x_address" => $address-1,
"x_state" => $state,
"x_response_format" => "1",
"x_intervalLength" => "10",
"x_start_date" => date('Y-m-d'),
"x_totalcycles" => "10",
"x_zip" => $zip
// Additional fields can be added here as outlined in the AIM integration
// guide at: http://developer.authorize.net
);
$invoice_no= $post_values['x_invoice_num'];
$transaction_no= $post_values['x_trans_id'];

$post_string = "";
foreach( $post_values as $key => $value )$post_string .= "$key=" . urlencode( $value ) . "&";
$post_string = rtrim($post_string,"& ");


//for test mode use the followin url
if($mode == 'test'){
$post_url = "https://test.authorize.net/gateway/transact.dll";
} else {
	$post_url = "https://secure.authorize.net/gateway/transact.dll";
}
//for live use this url
//$post_url = "https://secure.authorize.net/gateway/transact.dll";

$request = curl_init($post_url); // initiate curl object
curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
$post_response = curl_exec($request); // execute curl post and store results in $post_response
// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
curl_close ($request); // close curl object

// This line takes the response and breaks it into an array using the specified delimiting character
$response_array = explode($post_values["x_delim_char"],$post_response);
		 /*if($post_response) {
		  $email->send('thankyou', $post_values);
		  $email->send('notification', $post_values);
		 }*/
 exit;
	 } 

	
 $card_number = str_replace(" ","",$creditcard);
$arr_subscription = [
    'intervalLength' => 30,
    'start_date' => date('Y-m-d'),
    'totalcycles' => 10,
	'card_code' => $cvc,
    'amount' => $amount,
    'card_number' => $card_number,
    'expiry_date' => $exp,
    'first_name' => $first_name,
    'last_name' => $last_name,
	'address' => 'mahipat',
	'city' => $city,
	'state' => $state,
	'zip' => $zip,
	'country' => $country
];

//createSubscription($arr_subscription);
//echo "hello";
//exit;
createSubscription($arr_subscription);

function createSubscription($arr_data = [],$modechange = null)
{
	global $modechange;
	//$email = new Mail();
    //$log   = new Logger();
//echo "hello7";
//exit;
    extract($arr_data);
	//print_r($arr_data);
    /* Create a merchantAuthenticationType object with authentication details
       retrieved from the constants file */
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(ANET_LOGIN_ID);
    $merchantAuthentication->setTransactionKey(ANET_TRANSACTION_KEY);
    
    // Set the transaction's refId
    $refId = 'ref' . time();
    // Subscription Type Info
    $subscription = new AnetAPI\ARBSubscriptionType();
    $subscription->setName("Sample Subscription");
    $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
    $interval->setLength($intervalLength);
    $interval->setUnit("days");
    $paymentSchedule = new AnetAPI\PaymentScheduleType();
    $paymentSchedule->setInterval($interval);
    $paymentSchedule->setStartDate(new DateTime($start_date));
    $paymentSchedule->setTotalOccurrences($totalcycles);
    //$paymentSchedule->setTrialOccurrences("1");
    $subscription->setPaymentSchedule($paymentSchedule);
    $subscription->setAmount($amount);
	
    //$subscription->setTrialAmount("0.00");
    
    $creditCard = new AnetAPI\CreditCardType();
    $creditCard->setCardNumber($card_number);
    $creditCard->setExpirationDate($expiry_date);
	$creditCard->setcardCode($card_code);
	
    $payment = new AnetAPI\PaymentType();
		
    $payment->setCreditCard($creditCard);
    $subscription->setPayment($payment);
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber(mt_rand(10000, 99999));   //generate random invoice number     
    $order->setDescription("Daily Subscription For 1 USD"); 
    $subscription->setOrder($order); 
    
    $billTo = new AnetAPI\NameAndAddressType();
    $billTo->setFirstName($first_name);
    $billTo->setLastName($last_name);
	$billTo->setaddress($address);
    $billTo->setcity($city);
	$billTo->setstate($state);
    $billTo->setzip($zip);
	$billTo->setcountry($country);
    $subscription->setBillTo($billTo);
    $request = new AnetAPI\ARBCreateSubscriptionRequest();
    $request->setmerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setSubscription($subscription);
	
    print_r($request);
	//echo "ab".$amount;
	//exit;	
    $controller = new AnetController\ARBCreateSubscriptionController($request);

	
		//print_r($request);
	
	//$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
	//print_r($response);
	if ($modechange == 'test') {
    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
    } else {
	$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);	
	}
    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
    {
        echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
     }
    else
    {
        echo "ERROR :  Invalid response\n";
        $errorMessages = $response->getMessages()->getMessage();
        echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
    }
    return $response;
	
	
}
?>
