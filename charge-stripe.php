<?php

require_once('config-stripe.php');
require_once('mailer.php');
require_once('logger.php');

// TODO:
// * try/catch charges

add_action( 'wp_ajax_nopriv_charge_stripe', 'charge_stripe_cb' );
add_action( 'wp_ajax_charge_stripe', 'charge_stripe_cb' );

function charge_stripe_cb() {

  if ( isset($_POST['formData']) ) {

    // parse ajax data into variables
    parse_str( json_decode(stripslashes($_POST['formData']), TRUE) );

    // exit($email_address);

    echo $_POST['formData'];

    /**
    * Validation
    * 
    * `$amount`: as a real number (required).
    * `$token`: as present (required).
    * `$stripe_nonce`: as hash string match (required).
    * `$_wp_http_referer`: as appropriate origin url (required).
    */
    $valid = ( !empty($amount) && (int)$amount == $amount && $amount % 1 == 0 ) ? true : false;
    $valid = ( $valid === true && !empty($token) ) ? true: false;
    $valid = ( $valid === true && wp_verify_nonce($stripe_nonce, 'stripe_nonce') ) ? true : false;
    // $valid = ( $valid === true && ) ? true : false;
    if (!$valid) return false;

    // email object
    $email = new Mail();
    $log   = new Logger();

    // check nonce and referrer token
    // check_ajax_referer( $_wp_http_referer, '_wp_http_referer' ) 

    if ( $recurring != 'monthly') { // bill once

      $data = array(
        'amount' =>  $amount,
        'donation_type' => 'one-time',
        'donor_email' => $email_address,
        'firstname' => $firstname,
        'lastname' => $lastname
      );

      $charge = \Stripe\Charge::create(array(
        "amount" => $amount * 100, // amount in cents
        "currency" => "usd",
        "source" => $token,
        "description" => "One-time charge for $email_address"
      ));

      // email and log
      if ( $charge ){
        $email->send('thankyou', $data);
        $email->send('notification', $data);
     // $log->write('stripe', $data);

      } else {
        $email->transmit('support', $data);
     // $log->write('stripe', $data);
      }

    } else { // recurring bill
      // recurring payments: https://stripe.com/docs/subscriptions/quickstart

      $data = array(
        'amount' =>  $amount,
        'donation_type' => 'one-time',
        'donor_email' => $email_address,
        'firstname' => $firstname,
        'lastname' => $lastname
      );
      
      $uniqid = uniqid('donation_');
      $plan = \Stripe\Plan::create(array(
        "name" => $uniqid,
        "id" => $uniqid,
        "interval" => "month",
        "currency" => "usd",
        "amount" => $amount * 100, // amount in cents
      ));

      $customer = \Stripe\Customer::create(array(
        "source" => $token,
        "description" => "Monthly charge for $email_address"
      ));

      $subscription = \Stripe\Subscription::create(array(
        "customer" => $customer->id,
        "items" => array( array( "plan" => $plan->id ) ),
      ));

      // email and log
      if ( $subscription ){
        $email->transmit('thankyou', $data);
        $email->transmit('notification', $data);
      } else {
        $email->transmit('support', $data);
      }

    }
  }

}