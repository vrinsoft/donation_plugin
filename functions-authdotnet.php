<?php

require 'vendor/autoload.php';
require 'config-authdotnet.php';
//require 'charge-authdotnet.php';

// [authdotnet_payment_form]
function authdotnet_payment_form_cb(){
	
	/*ashi added*/
	wp_enqueue_style( 'donation-styles', plugins_url() . '/donations/templates/styles.css' );
	wp_enqueue_style( 'select-boxit-styles', 'http://gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css' );
	require 'templates/authdotnet-form.php';
	wp_enqueue_script( 'authdotnet-v2-js', 'https://jstest.authorize.net/v1/Accept.js' );
	wp_enqueue_script( 'authdotnet-v3-js', 'https://js.authorize.net/v1/Accept.js' );
	wp_enqueue_script( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', array('jquery') );
	wp_enqueue_script( 'jquery-validator', '//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js', array('jquery') );
	wp_enqueue_script( 'select-boxit-js', 'http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js' );
	wp_enqueue_script( 'cleave-js', plugins_url() . '/donations/js/cleave.min.js' );
	wp_enqueue_script( 'donations-fontawesome', 'https://use.fontawesome.com/446ba55cfa.js' );
	/* ashi added end */
	wp_enqueue_script( 'form-submission-js', plugins_url() . '/donations/js/submit-form-authdotnet.js' );
	
		// get publishable key of active mode (default: test mode)
	$publishable_key = (get_option('gateway_mode') != 'live')
		? get_option('authdotnet_sandbox_api_id')
		: get_option('authdotnet_live_api_id');
    
	$token_key = (get_option('gateway_mode') != 'live')
		? get_option('authdotnet_sandbox_transaction_key')
		: get_option('authdotnet_live_transaction_key');
    
	// get minimum donation amount (default: $2.00)
	$minimum_amount = ( empty(get_option('minimum_donation_amount')) )
		? 2 : get_option('minimum_donation_amount');
    /*if((get_option('gateway_mode') != 'live')) {
	  $mode = get_option('gateway_mode');	
	} else {
		
	  $mode = get_option('gateway_mode'); 	
	}*/
	$mode = (get_option('gateway_mode') != 'live')
	  ? get_option('gateway_mode')
	  : get_option('gateway_mode');
	
	// get wrapper element to append screen overlay (default: #page)
	$wrapper_element = ( empty( get_option('wrapper_element')) )
		? '#page' : get_option('wrapper_element');

	$siteurl =  get_site_url();
	/*wp_localize_script( 'form-submission-js', 'authdotnet', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	));*/
	wp_localize_script( 'form-submission-js', 'authdotnet', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'publishable_key' => $publishable_key,
		'transaction_key' => $token_key,
		'minimum_amount'  => $minimum_amount,
		'siteurl' => $siteurl,
		'mode' => $mode,
		'wrapper_element' => $wrapper_element
	));
}
add_shortcode( 'authdotnet_payment_form', 'authdotnet_payment_form_cb' );
