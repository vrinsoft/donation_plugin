<?php

	$stripe = array(
		'test_secret_key'      =>   get_option('stripe_test_secret_key'),
	  	'test_publishable_key' =>  get_option('authdotnet_live_transaction_key'),
	  	'live_secret_key' 	   =>  get_option('authdotnet_sandbox_api_id'),
	  	'live_publishable_key' =>  get_option('authdotnet_sandbox_transaction_key')
	);

	// yeah, it's a ternary if you can't see it at first glance
	( get_option('gateway_mode') != 'live' )
	? \Stripe\Stripe::setApiKey($stripe['test_secret_key'])
	: \Stripe\Stripe::setApiKey($stripe['live_secret_key']);