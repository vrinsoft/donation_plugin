;(function(document, window, undefined){

  /**
  * Localized to this script from PHP:
  *
  * `stripe.ajaxurl`
  * `stripe.publishable_key`
  * `stripe.minimum_amount`
  * `stripe.wrapper_element`
  */

  // move this out to another file V


	var modal = document.getElementById('modal');
	document.querySelector(stripe.wrapper_element).prepend(modal);

	// backdrop for closing modal
	var screenEl = document.createElement('div');
	document.querySelector(stripe.wrapper_element).prepend(screenEl);
	screenEl.id = 'screen';

	// close modal
	jQuery('#screen, #modal button').click(function(){
	  jQuery('#screen, #modal').hide();
	});

	var initialForm = document.getElementById('stripe-initial-form');
	var modalForm 	= document.getElementById('stripe-payment-form');
	var submit 		= document.getElementById('submit-stripe');
	var errors 		= document.getElementById('payment-errors');

	try {

		Stripe.setPublishableKey(stripe.publishable_key);

		// submit handler for initial-form
		initialForm.addEventListener('submit', function(e){
			var validEntry = undefined;
			e.preventDefault();

			// validate empty amount field with distinct message

			// validate payment amount as `present` and `real` number
			( this.elements[0].value == '' || isFloat(this.elements[0].value) )
				? ( validEntry = false, initialFormErrorMsg('Amount must be a whole number.') )
				: validEntry = true;

			// validate minimum payment amount to prevent fraudulent cc testing
			( this.elements[0].value < Number(stripe.minimum_amount) && !isFloat(this.elements[0].value) )
				? ( validEntry = false, initialFormErrorMsg('The minimum amount is '+stripe.minimum_amount+'.') )
				: (validEntry != false || validEntry == undefined) ? validEntry = true : validEntry = false;

			// insert intitial-form values (`amount` and `recurring`) into modal-form
			if ( validEntry ) {
				modalForm.elements[0].value = this.elements[0].value;
				( this.elements[1].checked )
					? modalForm.elements[1].value = this.elements[1].value
					: modalForm.elements[1].value = '';

				jQuery('#screen, #modal').show();
				//jQuery('#stripe-initial-form input:not([type="submit"])').val(''); // clear form
				return false;
			}
		});


		// modal form - handler
		modalForm.addEventListener('submit', function(e){
			var exp = document.querySelector('.stripe-exp').value.split('/');
			e.preventDefault();
			submit.setAttribute('disabled', true);
			Stripe.card.createToken({
				'number': document.querySelector('input[data-stripe="number"]').value.split(' ').join(''),
				'cvc' : document.querySelector('input[data-stripe="cvc"]').value,
				'exp_month' : exp[0],
				'exp_year' : exp[1],
				'address_zip' : document.querySelector('input[data-stripe="zip"]').value,
			}, stripeResponseHandler);
			//jQuery('#stripe-payment-form input:not([type="submit"])').val(''); // clear form
			return false;
		});

		// // handlers
		function isFloat(n) {
			return (Number(n) !== n && n % 1 !== 0);
		}

		function initialFormErrorMsg(error) {
			if ( !jQuery('#stripe-initial-form div#error-msg').length ) {
        jQuery('#stripe-initial-form').prepend('<div id="error-msg" class="alert alert-danger" style="color: black;">'+error+'</div>')
        .show('slow', function(){
          setTimeout(function(){
            jQuery('#stripe-initial-form div#error-msg').remove();
          }, 4000);
        });
      }
		}

		function stripeResponseHandler(status, response) {
			if (response.error){
				errors.innerText = response.error.message;
				submit.removeAttribute('disabled');
			} else {
				errors.innerText = ''; // cleanup old error messages
				var formData = jQuery('#stripe-payment-form').serialize();
				var token = response.id;
				var data  = formData + '&token=' + token; // append the token

				jQuery.ajax({
					url: stripe.ajaxurl,
					type: 'post',
					data: {
						'action': 'charge-stripe',
						'formData': JSON.stringify(data)
					}, 
					success: function(response){
						console.log(response);
						submit.removeAttribute('disabled');
					},
					error: function(e){
						console.log(e);
						submit.removeAttribute('disabled');
					}
				});
			}
		}

	} catch(e) {
		console.log(e);
	}


	/**
	* Specialized form handlers using Cleave.
	*/

	// credit card # formatting and recognition
	new Cleave('.stripe-cc', {
	    creditCard: true, 
	    onCreditCardTypeChanged: function (type) {
	    	if ( type !== 'unknown' ) {
	    		document.querySelector('.recognized-card').innerText = type.toUpperCase();
	    	} else {
	    		document.querySelector('.recognized-card').innerText = '';
	    	}
    	}
	});

	// date formatting
	new Cleave('.stripe-exp', {
    	date: true,
    	datePattern: ['m', 'Y']
	});

	// selectbox styling for long data. remove prior styles.
    jQuery('select[data-stripe="country"]').selectBoxIt();
	jQuery('span.selectboxit.country.selectboxit-enabled.selectboxit-btn').removeAttr('style');


})(document, window);