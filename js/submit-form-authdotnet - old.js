jQuery(document).ready(function(){

  /**

  */

  // move this out to another file V


	var modal = document.getElementById('modal');
	document.querySelector(authdotnet.wrapper_element).prepend(modal);

	// backdrop for closing modal
	var screenEl = document.createElement('div');
	document.querySelector(authdotnet.wrapper_element).prepend(screenEl);
	screenEl.id = 'screen';

	// close modal
	jQuery('#screen, #modal button').click(function(){
	  jQuery('#screen, #modal').hide();
	});

	var initialForm = document.getElementById('autho-initial-form');
	var modalForm 	= document.getElementById('authorize-payment-form');
	var submit 		= document.getElementById('submit-authorize');
	var errors 		= document.getElementById('payment-errors');

	//try {

		//auth.setPublishableKey(auth.publishable_key);

		// submit handler for initial-form
		initialForm.addEventListener('submit', function(e){
			var validEntry = undefined;
			e.preventDefault();

			// validate empty amount field with distinct message

			// validate payment amount as `present` and `real` number
			( this.elements[0].value == '' || isFloat(this.elements[0].value) )
				? ( validEntry = false, initialFormErrorMsg('Amount must be a whole number.') )
				: validEntry = true;

			// validate minimum payment amount to prevent fraudulent cc testing
			( this.elements[0].value < Number(authdotnet.minimum_amount) && !isFloat(this.elements[0].value) )
				? ( validEntry = false, initialFormErrorMsg('The minimum amount is '+authdotnet.minimum_amount+'.') )
				: (validEntry != false || validEntry == undefined) ? validEntry = true : validEntry = false;

			// insert intitial-form values (`amount` and `recurring`) into modal-form
			if ( validEntry ) {
				modalForm.elements[0].value = this.elements[0].value;
				( this.elements[1].checked )
					? modalForm.elements[1].value = this.elements[1].value
					: modalForm.elements[1].value = '';

				jQuery('#screen, #modal').show();
				//jQuery('#stripe-initial-form input:not([type="submit"])').val(''); // clear form
				return false;
			}
		});
		
        // modal form - handler
		modalForm.addEventListener('submit', function(e){
			var exp = document.querySelector('.stripe-exp').value.split('/');
			alert(exp);
			e.preventDefault();
			
			submit.setAttribute('disabled', true);
			authdotnet({
				'number': document.querySelector('input[data-stripe="number"]').value.split(' ').join(''),
				'cvc' : document.querySelector('input[data-stripe="cvc"]').value,
				'exp_month' : exp[0],
				'exp_year' : exp[1],
				'address_zip' : document.querySelector('input[data-stripe="zip"]').value,
			}, authrizeResponseHandler);
			//jQuery('#stripe-payment-form input:not([type="submit"])').val(''); // clear form
			return false;
		});
		modalForm.addEventListener("submit", authrizeResponseHandler);
		//jQuery("#submit-authorize").on('click',authrizeResponseHandler);

		
		// // handlers
		function isFloat(n) {
			return (Number(n) !== n && n % 1 !== 0);
		}

		function initialFormErrorMsg(error) {
			if ( !jQuery('#autho-initial-form div#error-msg').length ) {
        jQuery('#autho-initial-form').prepend('<div id="error-msg" class="alert alert-danger" style="color: black;">'+error+'</div>')
        .show('slow', function(){
          setTimeout(function(){
            jQuery('#autho-initial-form div#error-msg').remove();
          }, 4000);
        });
      }
		}
		
		
		function authrizeResponseHandler(status, response) {
       
		var creditcard = jQuery("#creditcard").val();
		var cvc = jQuery("#cvc").val();
		var cardholder = jQuery("#cardholder").val();
		var exp = jQuery("#exp").val();
		var address = jQuery("#address").val();
        var city = jQuery("#city").val();
		var zip = jQuery("#zip").val();
		var country = jQuery("#country").val();
		var state = jQuery("#state").val();
		var firstname = jQuery("#firstname").val();
		var lastname = jQuery("#lastname").val();
		var email = jQuery("#email").val();
		if(creditcard == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#creditcard').focus();
                jQuery('#creditcard').addClass("redBorderDav");
                jQuery('.error-creditcard').html("Please Fill the Credit Card Number");
                jQuery('.error-creditcard').fadeIn();
                jQuery('.error-creditcard').fadeOut(2000);
		   return false;
		} else if(cvc == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#cvc').focus();
                jQuery('#cvc').addClass("redBorderDav");
                jQuery('.error-cvc').html("Please Enter CVV number");
                jQuery('.error-cvc').fadeIn();
                jQuery('.error-cvc').fadeOut(2000);
		   return false;
		} else if(cardholder == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#cardholder').focus();
                jQuery('#cardholder').addClass("redBorderDav");
                jQuery('.error-cardholder').html("Please Enter card holder name");
                jQuery('.error-cardholder').fadeIn();
                jQuery('.error-cardholder').fadeOut(2000);
		   return false;
		} else if(exp == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#exp').focus();
                jQuery('#exp').addClass("redBorderDav");
                jQuery('.error-exp').html("Please Enter card Exp date");
                jQuery('.error-exp').fadeIn();
                jQuery('.error-exp').fadeOut(2000);
		   return false;
		} else if(address == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#address').focus();
                jQuery('#address').addClass("redBorderDav");
                jQuery('.error-address').html("Please Enter Address");
                jQuery('.error-address').fadeIn();
                jQuery('.error-address').fadeOut(2000);
		   return false;
		} else if(city == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#city').focus();
                jQuery('#city').addClass("redBorderDav");
                jQuery('.error-city').html("Please Enter city name");
                jQuery('.error-city').fadeIn();
                jQuery('.error-city').fadeOut(2000);
		   return false;
		} else if(zip == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#zip').focus();
                jQuery('#zip').addClass("redBorderDav");
                jQuery('.error-zip').html("Please Enter zip code");
                jQuery('.error-zip').fadeIn();
                jQuery('.error-zip').fadeOut(2000);
		   return false;
		} else if(country == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#country').focus();
                jQuery('#country').addClass("redBorderDav");
                jQuery('.error-country').html("Please Enter country");
                jQuery('.error-country').fadeIn();
                jQuery('.error-country').fadeOut(2000);
		   return false;
		} else if(state == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#state').focus();
                jQuery('#state').addClass("redBorderDav");
                jQuery('.error-state').html("Please Enter state name");
                jQuery('.error-state').fadeIn();
                jQuery('.error-state').fadeOut(2000);
		   return false;
		} else if(firstname == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#firstname').focus();
                jQuery('#firstname').addClass("redBorderDav");
                jQuery('.error-firstname').html("Please Enter first name");
                jQuery('.error-firstname').fadeIn();
                jQuery('.error-firstname').fadeOut(2000);
		   return false;
		} else if(lastname == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#lastname').focus();
                jQuery('#lastname').addClass("redBorderDav");
                jQuery('.error-lastname').html("Please Enter last name");
                jQuery('.error-lastname').fadeIn();
                jQuery('.error-lastname').fadeOut(2000);
		   return false;
		} else if(email == ""){
		       //jQuery(".error-creditcard").html("Please Fill the Credit Card Number");
		       jQuery('#email').focus();
                jQuery('#email').addClass("redBorderDav");
                jQuery('.error-email').html("Please Enter Email address");
                jQuery('.error-email').fadeIn();
                jQuery('.error-email').fadeOut(2000);
		   return false;
		} else{
			//console.log(jQuery("#authorize-payment-form input"));
			var inputs = jQuery("#authorize-payment-form input");
			var validated = false;
			jQuery.each(inputs,function(i,v){
				if(jQuery(v).val() == ""){
					jQuery(v).css('{"border-color","red"}');
					validated = false;
					//return false;
				}
			});
		//if(jQuery(".cc-num input").val()!= ""){}
				errors.innerText = ''; // cleanup old error messages
				var authkey = authdotnet.publishable_key;
				formData = jQuery('#authorize-payment-form').serialize();				
				
				//var token = response.id;
				//var data  = formData + '&token=' + token; // append the token

				jQuery.ajax({
					url: authdotnet.siteurl +"/wp-content/plugins/donations/charge-authorize.php",
					type: 'POST',
					data: formData,
					success: function(response){
						//alert(response);
						console.log(response);
						submit.removeAttribute('disabled');
					},
					error: function(e){
						console.log(e);
						submit.removeAttribute('disabled');
					}
				});
				return true;
			}		

		}


	// } catch(e) {
	// 	console.log(e);
	// }


	/**
	* Specialized form handlers using Cleave.
	*/

	// credit card # formatting and recognition
	 new Cleave('.stripe-cc', {
	     creditCard: true, 
	     onCreditCardTypeChanged: function (type) {
	     	if ( type !== 'unknown' ) {
	     		document.querySelector('.recognized-card').innerText = type.toUpperCase();
	     	} else {
	     		document.querySelector('.recognized-card').innerText = '';
	     	}
     	}
	 });

	// // date formatting
	 new Cleave('.stripe-exp', {
     	date: true,
     	datePattern: ['m', 'Y']
	 });

	// // selectbox styling for long data. remove prior styles.
     jQuery('select[data-stripe="country"]').selectBoxIt();
	 jQuery('span.selectboxit.country.selectboxit-enabled.selectboxit-btn').removeAttr('style');
});
